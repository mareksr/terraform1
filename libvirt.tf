# instance the provider
provider "libvirt" {
  uri = "qemu:///system"


}

variable "ssh_user" {
default = "root"
}

variable  "ssh_authorized_key" {

default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDD5Qz0Z+NEbXCVZjOdTyKreuhl8rdVvLbG+rr+I9F3rVbe5QsiPF4l7JSBfnFH0O1Vj9k9xOulwfs/8Oc2uLpHKp8TlF2v4oP4CiN0WUsKBP3U/PyCc3gODV3DlnVYuyBSv450LJi5rnkc2fPAYK0BSDPbP/iBS/ga6G78pGu86TRo4L11btE1e1yEJ2bNUmyGYJPzeYlt0imN4B2xyH8DpsvdEhG9SdNjv1KXVAivRHkTkGMEgwFYmWcV4kMZTabbh03L47SfApNV/fAPfpPPAM26nvuW5qk1vs49E0pDeeYtxLbDQlHoJq4GBBfFvClx1jfWJrZ9xOveG46J6/hH root@baltazar1.hb.pl"

}

# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "centos-qcow2" {
  name = "centos-qcow2"
  pool = "vm-images" #CHANGE_ME
  source = "http://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1901.qcow2"
  format = "qcow2"
}

# Create a network for our VMs
#resource "libvirt_network" "vm_network" {
#   name = "vm_network"
#   addresses = ["192.168.122.0/24"]
#}

# Use CloudInit to add our ssh-key to the instance
resource "libvirt_cloudinit_disk" "commoninit" {
          name           = "commoninit.iso"
          pool = "vm-images"
	   user_data          = "${data.template_file.user_data.rendered}"
        }
data "template_file" "user_data" {
  template = "${file("${path.module}/cloud_init.cfg")}"
}



# Create the machine
resource "libvirt_domain" "domain-centos" {
  name = "centos-terraform"
  memory = "512"
  vcpu = 1
 cloudinit = "${libvirt_cloudinit_disk.commoninit.id}"


  network_interface {
    hostname = "master"
    #network_name = "vm_network"
    network_name = "default"
	wait_for_lease = "true"

  }

  # IMPORTANT
  # Ubuntu can hang is a isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
      type        = "pty"
      target_type = "virtio"
      target_port = "1"
  }

  disk {
       volume_id = "${libvirt_volume.centos-qcow2.id}"
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}

# Print the Boxes IP
# Note: you can use `virsh domifaddr <vm_name> <interface>` to get the ip later
output "ip" {
  value = "${libvirt_domain.domain-centos.network_interface.0.addresses.0}"
}
#
